import Window from 'window';

import { scopedAttributes } from '../index.js';

describe('scopedAttributes', function () {
  beforeEach(function () {
    const { document } = new Window();
    this.element = document.createElement('button');
  });

  it('reads a single attribute', function () {
    this.element.setAttribute('data-parameter', 'value');
    expect(scopedAttributes(this.element, 'parameter')).to.eql('value');
  });

  it('reads multiple attributes', function () {
    this.element.setAttribute('data-scope-parameter', 'scoped');
    this.element.setAttribute('data-parameter', 'value');
    expect(scopedAttributes(this.element, ['scope', 'parameter'])).to.eql([
      'scoped',
      'value',
    ]);
  });

  it('filters out empty values', function () {
    this.element.setAttribute('data-scope-parameter', 'scoped');
    expect(scopedAttributes(this.element, ['scope', 'parameter'])).to.eql([
      'scoped',
    ]);
  });

  describe('options.attributeName', function () {
    it('configures how the scope turns into a data attribute', function () {
      this.element.setAttribute('data-action-param', 'value');

      function attributeName(scopes) {
        expect(scopes).to.eql('action');
        return `data-${scopes}-param`;
      }

      expect(
        scopedAttributes(this.element, 'action', { attributeName }),
      ).to.eql('value');
    });
  });

  describe('options.getAttribute', function () {
    it('configures how attribute is read from the element', function () {
      this.element.setAttribute('data-param', JSON.stringify({ key: 'value' }));

      function getAttribute(element, attributeName) {
        const stringValue = element.getAttribute(attributeName);
        if (stringValue) {
          return JSON.parse(stringValue);
        }
      }
      expect(scopedAttributes(this.element, 'param', { getAttribute })).to.eql({
        key: 'value',
      });
    });
  });
});
