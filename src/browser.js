/**
 * Entry point for the browser,
 * gathering all exports within a unique object
 *
 */
import * as api from './index.js';

export default api;
