// Main ESM file
/**
 *
 * @param {HTMLElement} element
 * @param {String|Array.<String>} scopes
 */
export function scopedAttributes(
  element,
  scopes,
  {
    attributeName = scopedAttributeName,
    getAttribute = defaultGetAttribute,
  } = {},
) {
  if (Array.isArray(scopes)) {
    return scopes
      .map((_, index) => {
        return getAttribute(element, attributeName(...scopes.slice(index)));
      })
      .filter(Boolean);
  }

  return getAttribute(element, attributeName(scopes));
}

export function scopedAttributeName(...scopes) {
  return `data-${scopes.join('-')}`;
}

export function defaultGetAttribute(element, attributeName) {
  return element.getAttribute(attributeName);
}
